import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { AnimationItem } from 'lottie-web';

@Component({
  selector: 'app-ball-anim',
  templateUrl: 'ball-animation.component.html'
})

export class BallAnimationComponent implements OnInit {
  options: AnimationOptions = {
    path: '/assets/lottie/teching.json'
  }

  constructor() { }

  ngOnInit() { }

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }
}
