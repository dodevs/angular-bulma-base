import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoPanelComponent } from './components/info-panel/info-panel.component';
import { DashboardComponent } from './pages/dashboard.component';
import { BallAnimationComponent } from './components/ball-animation/ball-animation.component';
import { LottieComponent, LottieModule } from 'ngx-lottie';

import player from 'lottie-web';

const ROUTES: Routes = [
    { path: '', component: DashboardComponent }
]

/* Lottie factory function */
export function playerFactory() {
  return player;
}

@NgModule({
    declarations: [
        DashboardComponent,
        InfoPanelComponent,
        BallAnimationComponent
    ],
    imports: [
        RouterModule.forChild(ROUTES),
        LottieModule.forRoot({player: playerFactory})
    ],
    exports: [

    ]
})
export class DashboardModule {}
